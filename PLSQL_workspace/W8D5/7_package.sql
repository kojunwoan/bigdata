create or replace procedure lotto_sal(
  vempno in emp.empno%type,
  vratio in number
)
-- desc dbms_random
-- select trunc(dbms_random.value(0,10),0) from emp;
is
 vsal emp.sal%type;
 vcheck number;
 persent number := 1;
begin
  vcheck := trunc(dbms_random.value(0,10),0);
  dbms_output.put_line('vcheck : ' || vcheck);
  if vcheck in (2,5,7,9) then
    dbms_output.put_line('성공');    
    persent := (100+vratio)/100;
  elsif vcheck in (1,4,8,0) then
  dbms_output.put_line('실패');
    persent := (100-vratio)/100;
  else
  dbms_output.put_line('꽝! 다음 기회에');    
  end if;
  select sal
    into vsal
    from emp
   where empno = vempno;
  dbms_output.put_line(vempno || '번 사원의 급여 : ' || vsal);
   vsal := vsal*persent;
   update emp
      set sal = vsal
    where empno = vempno;
  dbms_output.put_line('최종 급여 : ' || (vsal));
end;
/