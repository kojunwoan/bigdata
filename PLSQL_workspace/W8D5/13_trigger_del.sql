create or replace trigger delete_tri
after delete on dept
for each row
begin
    delete from cdept
     where deptno = :old.deptno;
end;
--delete from dept where deptno = 19;
--트리거 필요 없으니 지웁시다.
--select trigger_name from user_triggers;
--drop trigger delete_tri;
/
