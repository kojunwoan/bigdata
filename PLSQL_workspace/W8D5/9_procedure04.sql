create or replace procedure LOTTO_BONUS(
  vempno in emp.empno%type
)
is 
  vename emp.ename%type;
  vjob emp.job%type;
  vsal emp.sal%type;
  vcomm emp.comm%type;
begin
  select ename, job, sal, comm
  into vename, vjob, vsal, vcomm
  from emp
  where empno = vempno;

  insert into bonus
  values (vename, vjob, vsal, vcomm);

end;
/