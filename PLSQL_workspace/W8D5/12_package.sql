create or replace package body MY_PACK
is
    function find_annsal(
    vempno in emp.empno%type
    ) return number
    is
        vannsal emp.sal%type;
    begin
        select sal*12+nvl(comm,0)
        into vannsal
        from emp
        where empno = vempno;
        return vannsal;
    end find_annsal;


    procedure raise_sal2(
    vempno in emp.empno%type,
    percent in number
    )
    is 
        vsal emp.sal%type;
    begin
        select sal
        into vsal
        from emp
        where empno = vempno;

        dbms_output.put_line(vempno || '번 사원의 급여 : ' || vsal);
        dbms_output.put_line(percent || '%인상 급여 : ' || vsal*percent/100);
        vsal := vsal*(100+percent)/100;
        dbms_output.put_line('최종 급여 : ' || vsal);

        update emp
        set sal=vsal
        where empno = vempno;
    end raise_sal2;
end;
--select empno, my_pack.find_annsal(empno) from emp;
--execute my_pack.raise_sal2(7788,20);
/