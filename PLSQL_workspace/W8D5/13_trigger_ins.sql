-- trigger : 정해진 이벤트에 의해서 암시적으로 호출되는 pl/sql program unit
-- dept테이블에 데이터가 추가된후에 cdept 테이블에 이 내용을 추가
create or replace trigger ins_tri
-- 트리거가 동작할 이벤트에 대해서 기술
after insert on dept
--각 행마다
for each row
--begin 이하를 실행
begin
  insert into cdept
  values (:new.deptno, :new.dname, :new.loc);
end;
--insert into dept values (19,'corona','china');
/
