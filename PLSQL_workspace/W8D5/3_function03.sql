create or replace function find_dname(
  vempno in emp.empno%type
) return dept.dname%type
is
  vdname dept.dname%type;
begin
  select d.dname
    into vdname
    from emp e natural join dept d
   where e.empno = vempno;
   return vdname;
end;
/