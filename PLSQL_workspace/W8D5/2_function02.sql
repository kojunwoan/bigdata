-- create or replace
create function find_annsal(
  vempno in emp.empno%type
) return number
is
    vannsal emp.sal%type;
begin
  select sal*12+nvl(comm,0)
    into vannsal
  from emp
  where empno = vempno;
  return vannsal;
end;
-- desc find_annsal
-- select distinct name, type from user_source;
-- select text from user_source where name = 'FIND_ANNSAL';
/