create or replace procedure down_sal(
  vempno in emp.empno%type,
  persent in number
)
is
  vsal emp.sal%type;
begin
  select sal
    into vsal
    from emp
   where empno = vempno;

   vsal := vsal*(100-persent)/100;

   update emp
      set sal=vsal
    where empno = vempno;
end;
/