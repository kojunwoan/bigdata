-- 급여 10%인상하는 프로시져 RAISE_SAL
create or replace procedure raise_sal(
  vempno in emp.empno%type
)
is 
  vsal emp.sal%type;
begin
  select sal
    into vsal
    from emp
   where empno = vempno;

    vsal := vsal*1.1;

   update emp
      set sal=vsal
    where empno = vempno;
end;
--execute raise_sal(7788);
/