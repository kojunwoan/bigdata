create or replace procedure LOTTO_BONUS(
  vdeptno in emp.deptno%type
)
is
begin
  for c1 in (select ename, job, sal, comm
            from emp
            where deptno = vdeptno) loop  
    insert into bonus
    values (c1.ename, c1.job, c1.sal, c1.comm);
  end loop;
  commit;
end;
/