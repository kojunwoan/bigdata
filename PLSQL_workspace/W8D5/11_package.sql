--package 자주 사용되는 연관된 프로시져와 함수의 모음
-- 1. package spec 이름, 매개변수, 리턴타입 정보
-- 2. package body 소스코드
create or replace package MY_PACK
is
--package spec
--find_annsal 함수
function find_annsal(
  vempno in emp.empno%type
) return number;
--riase_sal2 프로시져
procedure raise_sal2(
  vempno in emp.empno%type,
  percent in number
);
end;
/ 