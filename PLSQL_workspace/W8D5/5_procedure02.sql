-- 급여 10%인상하는 프로시져 RAISE_SAL
create or replace procedure raise_sal2(
  vempno in emp.empno%type,
  percent in number
)
is 
  vsal emp.sal%type;
begin
  select sal
    into vsal
    from emp
   where empno = vempno;
    dbms_output.put_line(vempno || '번 사원의 급여 : ' || vsal);
    dbms_output.put_line(percent || '%인상 급여 : ' || vsal*percent/100);
    vsal := vsal*(100+percent)/100;
    dbms_output.put_line('최종 급여 : ' || vsal);

   update emp
      set sal=vsal
    where empno = vempno;
end;
--execute raise_sal(7788);
/