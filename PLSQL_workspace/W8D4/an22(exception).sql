-- exception
-- 1. pre define
-- 2. non pre define
-- 3. user define
accept vno prompt '사번입력 : '
declare
    vempno emp.empno%type := &vno;
    vename emp.ename%type;
    vsal emp.sal%type;
    --예외의 이름을 지정
    vex1 exception;
    vex2 exception;
    --컴파일러에게 예외가 발생할 때 예외명과 에러번호를 연결하라고 지시
    pragma exception_init(vex1, -06502);
begin
--ORA-06502
  -- vsal := 'SCOTT';
  select empno, ename, sal
    into vempno, vename, vsal
    from emp
   where empno = vempno;
   --사원의 급여 3000 이상이면 값을 출력
   --3000이하면 출력 X
  if vsal < 3000 then
    raise vex2;
  end if;

   dbms_output.put_line(vempno || '    ' || vename || '    ' || vsal);
exception
  when no_data_found then
    dbms_output.put_line('그런 사번은 존재하지 않습니다.');
  when too_many_rows then
    dbms_output.put_line('해당 사원이 둘 이상입니다.');
  when invalid_cursor then
    dbms_output.put_line('커서의 사용법이 부적합 합니다.');  
  when vex1 then
    dbms_output.put_line('숫자형 변수에 문자값은 할당할 수 없습니다.'); 
  when vex2 then
    dbms_output.put_line('월급은 비밀~~~!'); 
end;
/