
declare
-- cursor �Ӽ� %isopen->bool %found->bool %notfound->bool %rowcount->int(fetchȽ��)
  cursor c1 is
    select empno, ename, sal
      from emp
     where deptno = 10;
  c2 c1%rowtype;
begin
  open c1;
  loop
    fetch c1 into c2;
  exit when c1%notfound;
    dbms_output.put_line(c2.empno || '    ' || c2.ename || '    ' || c2.sal);
  end loop;
  close c1;
end;
/