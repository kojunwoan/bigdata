accept vdeptno prompt '몇번부서 : '
--반복문과 커서 결합
declare
  cursor c1 is
    select empno, ename, sal, deptno
      from emp
     where deptno = &vdeptno;
--   c2 c1%rowtype;
begin
--CURSOR FOR LOOP문 : FOR 변수명 IN CURSOR명 LOOP
    for c2 in c1 loop
        --open c1;
        --fetch c1 into c2;
        dbms_output.put_line(c2.empno || '    ' || c2.ename || '    ' || c2.sal || '    ' || c2.deptno);
    end loop;
    --close c1;
end;
/