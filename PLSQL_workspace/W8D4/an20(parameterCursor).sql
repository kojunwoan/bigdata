declare
--PARAMETER CURSOR
  cursor c1(vno number) is
    select empno, ename, sal, deptno
      from emp
     where deptno = vno;
begin
  for i in 1..3 loop
    for c2 in c1(i*10) loop
        dbms_output.put_line(c2.empno || '    ' || c2.ename || '    ' || c2.sal || '    ' || c2.deptno);
    end loop;
  end loop;
end;
/