--암시적 커서
begin
    for c2 in (select empno, ename, sal, deptno
              from emp
              where deptno = 10) loop
        dbms_output.put_line(c2.empno || '    ' || c2.ename || '    ' || c2.sal || '    ' || c2.deptno);
    end loop;
end;
/