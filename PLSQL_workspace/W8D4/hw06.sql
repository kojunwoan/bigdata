begin
    for c1 in (select empno, sal
              from emp
              where deptno = 10) loop
        update emp
           set sal = c1.sal*1.1
         where empno = c1.empno;      
    end loop;
    commit;
    
    --확인해보는 코드
    for c1 in (select empno, sal, deptno
      from emp
     where deptno = 10) loop
       dbms_output.put_line(c1.empno || '  ' || c1.sal || '  ' || c1.deptno);
     end loop;
end;
/