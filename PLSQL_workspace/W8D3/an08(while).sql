declare
  i number := 1;
begin
  while i<10 loop
    dbms_output.put_line('3 * ' || i || ' = ' || 3*i);
    i := i + 1;
  end loop;
end;
/