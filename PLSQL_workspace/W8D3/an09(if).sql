declare
    vsum number := 0;
begin
  for i in 1..100 loop
    if mod(i,3)=0 then
      --dbms_output.put_line(i);
      vsum := vsum + i;
    end if;
  end loop;
  dbms_output.put_line(vsum);
end;
/