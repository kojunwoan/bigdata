-- PL/SQL

-- 변수/ for/ if 가능

-- function
-- procedure
-- package
-- trigger

-- declare 선언부
-- begin 실행부
-- exception 예외처리부
-- end 종료부

-- 실행과 종료 필수
-- 나머지 선택

--set serveroutput on
--E:\app\user\product\11.2.0\dbhome_1\sqlplus\admin\glogin.sql
--sqlplus에 로그인 될때 자동으로 호출되는 파일

BEGIN
DBMS_OUTPUT.PUT_LINE('HELLO PL/SQL');
DBMS_OUTPUT.PUT_LINE('EUC-KR로 인코딩 변경');
DBMS_OUTPUT.PUT_LINE('한글 출력');
END;
/
