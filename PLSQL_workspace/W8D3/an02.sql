declare
--변수 선언하기
--변수명 타입
  VNO NUMBER;
  VSTR VARCHAR2(20);
begin
    --대입연산 :=
    VNO := 20;
    --vstr변수에 oracle 이라는 문자를 대입
    VSTR := 'oracle';
    dbms_output.put_line(VNO);
    --vstr변수의 값을 화면에 출력
    dbms_output.put_line(VSTR);
end;
/