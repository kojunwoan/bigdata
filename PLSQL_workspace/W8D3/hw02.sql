declare
    vempno number;
    vname varchar2(20);
    vjob varchar2(20);
    vdeptno number;
begin
  select empno, ename, job, deptno
    into vempno, vname, vjob, vdeptno
    from emp
   where empno = 7369;
  dbms_output.put_line(vempno|| '  ' ||vname|| '  ' ||vjob|| '  ' ||vdeptno);
end;
/