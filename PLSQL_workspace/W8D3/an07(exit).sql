declare
    i number := 0;
begin
  loop
    i := i + 1;
  exit when i>9;
    dbms_output.put_line('3 * ' || i || ' = ' || 3*i);
  end loop;
end;
/