declare
    vsum number :=0;
begin
  for i in reverse 1..100 loop
    dbms_output.put_line(i);
    vsum := vsum + i;
  end loop;
  dbms_output.put_line(vsum);
end;
/