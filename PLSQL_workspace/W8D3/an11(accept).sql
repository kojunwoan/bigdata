accept vno prompt '검색할 부서번호 입력:'
declare
    vdeptno number := &vno;
    --set verify off(구, 신 안뜸)
    vdname varchar2(20);
    vloc varchar2(20);
begin
  select dname, loc
    into vdname, vloc
    from dept
   where deptno = vdeptno;

   dbms_output.put_line(vdeptno || '       ' || vdname || '       ' || vloc);
end;
/